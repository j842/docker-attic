# attic #

Volumes:

* /sshkeys - maps to /root/.ssh, used for ssh 
* /data

Exposes port 22.

sudo docker run --rm -v "$(pwd)/sshkeys:/sshkeys" -v "$(pwd)/data:/data"-it attic /usr/bin/attic param1 param2 etc