# attic.

FROM phusion/baseimage
MAINTAINER j842

RUN apt-get update && apt-get install -y libfuse2 python3-llfuse python3-msgpack python3-openssl wget

RUN rm -f /etc/service/sshd/down
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

RUN cd /root && wget -qO- https://attic-backup.org/downloads/releases/0.16/Attic-0.16-linux-x86_64.tar.gz | tar zxv
RUN cd /root && mv Attic* attic
#RUN ln -s /opt/attic /root/Attic-0.16-linux-x86_64
RUN ln -s /root/attic/attic /bin/attic

RUN rm -rf /root/.ssh
RUN ln -s /root/.ssh /sshkeys

VOLUME ["/sshkeys","/data"]
EXPOSE 22
CMD ["/sbin/my_init"]
